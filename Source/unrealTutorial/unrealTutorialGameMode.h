// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "unrealTutorialGameMode.generated.h"

UCLASS(minimalapi)
class AunrealTutorialGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AunrealTutorialGameMode();
};



