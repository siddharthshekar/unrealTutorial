// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "unrealTutorial.h"
#include "unrealTutorialGameMode.h"
#include "unrealTutorialHUD.h"
#include "unrealTutorialCharacter.h"

AunrealTutorialGameMode::AunrealTutorialGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AunrealTutorialHUD::StaticClass();
}
