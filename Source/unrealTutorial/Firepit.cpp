// Fill out your copyright notice in the Description page of Project Settings.

#include "unrealTutorial.h"
#include "Engine.h"
#include "Firepit.h"


// Sets default values
AFirepit::AFirepit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	triggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("box"));
	triggerBox->bGenerateOverlapEvents = true;
	//triggerBox->OnComponentBeginOverlap.AddDynamic(this, &AFirepit::TriggerEnter);
	//triggerBox->OnComponentEndOverlap.AddDynamic(this, &AFirepit::TriggerExit);

	triggerBox->SetRelativeScale3D(FVector(3.0f, 3.0f, 1.0f));

	RootComponent = triggerBox;

	SM_Fire = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	SM_Fire->AttachTo(RootComponent);

	PS_Fire = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("particle"));
	PS_Fire->AttachTo(RootComponent);
	PS_Fire->SetVisibility(false);

	resetTime = 3;
}

// Called when the game starts or when spawned
void AFirepit::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AFirepit::AdvanceTimer, 1.0, true);
}

// Called every frame
void AFirepit::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (playerCharacter != NULL){

		if (playerCharacter->bIsInteracting && bPlayerIsWihtinRange && playerCharacter->Inventory.Contains("Matches") && !bFireisLit){

			Light();
		}
	
	}

}

void AFirepit::GetPlayer(AActor* player){

	playerCharacter = Cast<AunrealTutorialCharacter>(player);
}


void AFirepit::TriggerEnter(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){

	GetPlayer(otherActor);

	bPlayerIsWihtinRange = true;

	if (playerCharacter->Inventory.Contains("Matches")){

		if (!bFireisLit){
			GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, helpText);
		}
	
	}else{
	
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString::Printf(TEXT("You need matches to light fire")));
	}
}


void AFirepit::TriggerExit(class AActor* otherActor, class UPrimitiveComponent* otherComp, int32 otherBodyIndex){

	bPlayerIsWihtinRange = false;
}

void AFirepit::Light(){

	int32 xpGained = FMath::RandRange(10, 100);

	GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString::Printf(TEXT("You light the fire and gained %d xp"), xpGained));

	playerCharacter->Inventory.RemoveSingle("Matches");
	bFireisLit = true;
	PS_Fire->SetVisibility(true);

	playerCharacter->playerExperiance += xpGained;
}

void AFirepit::AdvanceTimer(){

	if (bFireisLit)
		--resetTime;
	if (resetTime < 1){
	
		TimerHasFinished();
	}

}

void AFirepit::TimerHasFinished(){

	PS_Fire->SetVisibility(false);
	bFireisLit = false;
	resetTime = 3;
}